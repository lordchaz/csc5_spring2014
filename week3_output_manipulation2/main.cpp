/* 
 * File:   main.cpp
 * Author: Charles Carsten
 *
 * Created on March 3, 2014, 3:27 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //create constant variable so i can not change it
    const double num =12.3294;
   // illegal code
    //num=12
    
    //default is right justified
    cout<<setw(10)<<num<<num<<endl;
    
    //one fix
    //cout<<setw(10); cout<< left<<num<<num<<endl;
    //fix 2 with <<brackets
    cout<<setw(10)<<left<<num<<num<<endl;
    
    //2 digits
    
    cout<<setprecision(2);
    cout<<setw(10)<<left<<num<<num<<endl;
    
    //3 decimal places
    //fixed sets setprecision to decimal places
    cout<<setprecision(3)<<fixed;
    cout<<setw(10)<<left<<num<<num<<endl;
    
    //setfill
    cout<<setfill('=');
    cout<<setw(10)<<num<<num<<endl;
    
   //using two setw
    cout<<setw(10)<<num<<setw(10)<<right<<num<<endl;
    cout<<setfill(' ');
       cout<<setw(10)<<num<<setw(10)<<right<<num<<endl;
    
    
    return 0;
}

