/* 
 * File:   main.cpp
 * Author: Charles Carsten
 *
 * Created on March 3, 2014, 4:18 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int a=5;
    int b=10;
    
    //craete new variable to store data
    int temp=a;
    cout<<"a;"<<a<<" b;"<<b<<endl;
    
    a=b;
    b=temp;
    
    cout<<"a;"<<a<<" b;"<<b<<endl;
    
    return 0;
}

